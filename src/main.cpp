#include <iostream>
#include <eonbridge.hpp>

using namespace std;



int main (int argc, char** argv) 
{	

	if(argc < 2)
	{
		cout << "You need to supply arguments: use --help for help." << endl;
      	return -1;
	}

	if(std::string(argv[1]) == "--help")
	{
		cout << "Arguments are processed in order:" << endl;
		cout << "  first the type of operation, admitted values: generator, prover, verifier" << endl;
		cout << "     generator:" << endl;
		cout << "        left: string, left 32bit words separated by comma" << endl;
		cout << "        right: string, right 32bit words separated by comma" << endl;
		cout << "        hash: string, right 32bit words separated by comma" << endl;
		cout << "        id: string, identifier for the files that will be created (keys)" << endl;
		cout << "        output path: string, path to the folder for the output files" << endl;
		cout << "     e.g. ./main generator left_words right_words hash_words my_test_id ../somefolder" << endl;
		cout << " " << endl;
		cout << "     prover:" << endl;
		cout << "        path to pk: string, file path to prover key" << endl;
		cout << "        path to vk: string, file path to verification key" << endl;
		cout << "        left: string, left 32bit words separated by comma" << endl;
		cout << "        right: string, right 32bit words separated by comma" << endl;
		cout << "        hash: string, right 32bit words separated by comma" << endl;
		cout << "        id: string, identifier for the files that will be created (proof)" << endl;
		cout << "     e.g. ./main prover path_to_pk path_to_vk left_words right_words hash_words my_test_id" << endl;
		cout << " " << endl;
		cout << "     verifier:" << endl;
		cout << "        path to pvk: string, file path to processed verification key" << endl;
		cout << "        path to proof: string, right 32bit words separated by comma" << endl;
		cout << "        hash: string, right 32bit words separated by comma" << endl;
		cout << "     e.g. ./main verifier path_to_pvk path_to_ptoof hash_words" << endl;
		cout << " " << endl;
	}
	else if (std::string(argv[1]) == "generator")
	{
		if(argc != 7) {
	        cout << "You need to supply exactly 5 arguments for the generator: left words, right words, control hash words, desired id for files, output folder path for files." << endl;
	        return -1;
	    }
	    string left = argv[2];
	    string right = argv[3];
	    string hash = argv[4];
	    string id = argv[5];
	    string output_path = argv[6];

	    generator(left, right, hash, id, output_path);
	    return 0;
	}
	else if (std::string(argv[1]) == "prover")
	{
		if(argc != 9) {
	        cout << "You need to supply exactly 7 arguments for the prover: path to pk, path to vk, left words, right words, control hash words, desired id for files, desired output folder." << endl;
	        return -1;
	    }
	    string pk_path = argv[2];
	    string vk_path = argv[3];
	    string left = argv[4];
	    string right = argv[5];
	    string hash = argv[6];
	    string id = argv[7];
	    string output_path = argv[8];

	    return prover_from_files(pk_path, vk_path, left, right, hash, id, output_path);
	}
	else if (std::string(argv[1]) ==  "verifier")
	{
		if(argc != 6) {
	        cout << "You need to supply exactly 4 arguments for the verifier: path to pvk, path to proof, control hash words, desired id for files - verifier only creates temporary files for now." << endl;
	        return -1;
	    }
	    string pvk_path = argv[2];
	    string proof_path = argv[3];
	    string hash = argv[4];
	    string id = argv[5];

	    return verifier_from_files(pvk_path, proof_path, hash, id);
	}
	
    
}
