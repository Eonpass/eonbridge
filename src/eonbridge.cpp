#include <fstream>
#include <stdio.h>
#include <cstdio>
#include <exception>

#include <libff/common/default_types/ec_pp.hpp>
#include <libsnark/common/default_types/r1cs_gg_ppzksnark_pp.hpp>
#include <libsnark/relations/constraint_satisfaction_problems/r1cs/examples/r1cs_examples.hpp>
#include <libsnark/zk_proof_systems/ppzksnark/r1cs_gg_ppzksnark/r1cs_gg_ppzksnark.hpp>

#include <libff/algebra/fields/field_utils.hpp>
#include <libff/common/default_types/ec_pp.hpp>
#include <libff/common/profiling.hpp>
#include <libff/common/utils.hpp>
#include <libsnark/common/default_types/r1cs_ppzksnark_pp.hpp>
#include <libsnark/gadgetlib1/gadgets/hashes/sha256/sha256_gadget.hpp>
#include <libsnark/gadgetlib1/pb_variable.hpp>
#include <libsnark/zk_proof_systems/ppzksnark/r1cs_ppzksnark/r1cs_ppzksnark.hpp>

#include "libff/algebra/curves/public_params.hpp"

using namespace libsnark;
using namespace std;
using namespace libff;


int _print_vk_to_file(r1cs_ppzksnark_verification_key<default_r1cs_ppzksnark_pp> vk, string pathToFile)
{
  std::filebuf fb;
  fb.open (pathToFile,std::ios::out);
  std::ostream os(&fb);
  os<<vk;
  fb.close();
  return 0;
}

int _print_pvk_to_file(r1cs_ppzksnark_processed_verification_key<default_r1cs_ppzksnark_pp> vk, string pathToFile)
{
  std::filebuf fb;
  fb.open (pathToFile,std::ios::out);
  std::ostream os(&fb);
  os<<vk;
  fb.close();
  return 0;
}

int _print_primaryinput_to_file(r1cs_primary_input<libff::Fr<default_r1cs_ppzksnark_pp>> primary_input, string pathToFile)
{
  std::filebuf fb;
  fb.open (pathToFile,std::ios::out);
  std::ostream os(&fb);
  os<<primary_input;
  fb.close();
  return 0;
}

int _print_pk_to_file(r1cs_ppzksnark_proving_key<default_r1cs_ppzksnark_pp> pk, string pathToFile)
{
  std::filebuf fb;
  fb.open (pathToFile,std::ios::out);
  std::ostream os(&fb);
  os<<pk;
  fb.close();
  return 0;
}

int _print_proof_to_file(r1cs_ppzksnark_proof<default_r1cs_ppzksnark_pp> proof, string pathToFile)
{
  std::filebuf fb;
  fb.open (pathToFile,std::ios::out);
  std::ostream os(&fb);
  os<<proof;
  fb.close();
  return 0;
}


r1cs_ppzksnark_verification_key<default_r1cs_ppzksnark_pp> _retrieve_vk_from_file(string pathToFile)
{
  r1cs_ppzksnark_verification_key<default_r1cs_ppzksnark_pp> vk;
  std::filebuf fb;
  if (fb.open (pathToFile,std::ios::in))
  {
    std::istream is(&fb);
    is>>vk;
    fb.close();
  }
  else cout << "Unable to open file"; 

  return vk;
}

r1cs_ppzksnark_processed_verification_key<default_r1cs_ppzksnark_pp> _retrieve_pvk_from_file(string pathToFile)
{
  r1cs_ppzksnark_processed_verification_key<default_r1cs_ppzksnark_pp> pvk;

  std::filebuf fb;
  if (fb.open (pathToFile,std::ios::in))
  {
    std::istream is(&fb);
    is>>pvk;
    fb.close();
  }
  else cout << "Unable to open file"; 
  return pvk;
}

r1cs_ppzksnark_proving_key<default_r1cs_ppzksnark_pp> _retrieve_pk_from_file(string pathToFile)
{
  r1cs_ppzksnark_proving_key<default_r1cs_ppzksnark_pp> pk;

  std::filebuf fb;
  if (fb.open (pathToFile,std::ios::in))
  {
    std::istream is(&fb);
    is>>pk;
    fb.close();
  }
  else cout << "Unable to open file"; 
  return pk;
}

r1cs_ppzksnark_proof<default_r1cs_ppzksnark_pp> _retrieve_proof_from_file(string pathToFile)
{
  r1cs_ppzksnark_proof<default_r1cs_ppzksnark_pp> proof;

  std::filebuf fb;
  if (fb.open (pathToFile,std::ios::in))
  {
    std::istream is(&fb);
    is>>proof;
    fb.close();
  }
  else cout << "Unable to open file"; 
  return proof;
}

r1cs_primary_input<libff::Fr<default_r1cs_ppzksnark_pp>> _retrieve_primary_input_from_file(string pathToFile)
{

  r1cs_primary_input<libff::Fr<default_r1cs_ppzksnark_pp>> p_input;

  std::filebuf fb;
  if (fb.open (pathToFile,std::ios::in))
  {
    std::istream is(&fb);
    is>>p_input;
    fb.close();
  }
  else cout << "Unable to open file"; 
  return p_input;
}

libff::bit_vector string_to_bits(string& input)
{
  string delimiter = ",";
  vector<unsigned long> hex_vector;

  size_t pos = 0;
  string token;
  auto start = 0U;
  auto end = input.find(delimiter);
  while (end != string::npos)
  {
      token = input.substr(start, end - start);
      unsigned long num = std::stol(token, 0, 16);
      hex_vector.push_back(num);
      start = end + delimiter.length();
      end = input.find(delimiter, start);
  }
  string last_token = input.substr(start, end);
  unsigned long num = std::stol(last_token, 0, 16);
  hex_vector.push_back(num);

  return libff::int_list_to_bits({hex_vector[0],hex_vector[1],hex_vector[2],hex_vector[3],hex_vector[4],hex_vector[5],hex_vector[6],hex_vector[7]}, 32);

}

int verifier(r1cs_ppzksnark_processed_verification_key<default_r1cs_ppzksnark_pp> pvk, r1cs_ppzksnark_proof<default_r1cs_ppzksnark_pp> proof, string& hash, string& id)
{
    //build the primary input from the expected hash:
    //default_r1cs_ppzksnark_pp::init_public_params();
    typedef libsnark::default_r1cs_ppzksnark_pp ppT;
    typedef libff::Fr<ppT> FieldT;
    const libff::bit_vector hash_bv = string_to_bits(hash);
    //write primary input to file and retrieve it to build the object
    string temp_file_name = string(id+"_temp_primary_input");
    ofstream tempinput;
    tempinput.open(temp_file_name);
    tempinput<<"256\n";
    for(const auto& value: hash_bv){
      tempinput<< value << "\n";
    }
    tempinput.close();
    r1cs_primary_input<FieldT> p_input = _retrieve_primary_input_from_file(temp_file_name);

    //verify
    bool verified = r1cs_ppzksnark_online_verifier_strong_IC<ppT>(pvk, p_input, proof);

    if(remove(temp_file_name.c_str())!=0){
      cout << "Error removing temporary file" << endl;
    }
    if(!verified){
      cout << "Verifier detects a corrupted proof" << endl;
    }

    return verified;
}

int verifier_from_files(string& pvk_path, string& proof_path, string& hash, string& id)
{
  try
  {
    default_r1cs_ppzksnark_pp::init_public_params();
    r1cs_ppzksnark_processed_verification_key<default_r1cs_ppzksnark_pp> pvk = _retrieve_pvk_from_file(pvk_path.c_str());
    r1cs_ppzksnark_proof<default_r1cs_ppzksnark_pp> proof = _retrieve_proof_from_file(proof_path.c_str());
    return verifier(pvk, proof, hash, id);
  }
  catch(int n)
  {
    return -1;
  }
  catch (const std::exception &exc)
  {
    std::cerr << exc.what();
    return -1;
  }
  catch(...)
  {
    return -1;
  }

}

r1cs_ppzksnark_proof<default_r1cs_ppzksnark_pp> prover(r1cs_ppzksnark_keypair<default_r1cs_ppzksnark_pp> keypair, string& left, string& right, string& hash, string& id, string& output_path)
{
    const libff::bit_vector left_bv = string_to_bits(left);
    const libff::bit_vector right_bv = string_to_bits(right);
    const libff::bit_vector hash_bv = string_to_bits(hash);
    //default_r1cs_ppzksnark_pp::init_public_params();
    typedef libsnark::default_r1cs_ppzksnark_pp ppT;
    typedef libff::Fr<ppT> FieldT;

    //setup protoboard
    protoboard<FieldT> pb;
    digest_variable<FieldT> hash_bits(pb, SHA256_digest_size, "hash_bits");
    pb.set_input_sizes(256);
    digest_variable<FieldT> left_bits(pb, SHA256_digest_size, "left_bits");
    digest_variable<FieldT> right_bits(pb, SHA256_digest_size, "right_bits");
    sha256_two_to_one_hash_gadget<FieldT> hasher(pb, left_bits, right_bits, hash_bits, "hash_gadget");
    hasher.generate_r1cs_constraints();

    //preapre witnesses
    left_bits.generate_r1cs_witness(left_bv);
    right_bits.generate_r1cs_witness(right_bv);
    hasher.generate_r1cs_witness();

    //data integrity check:
    if (hash_bits.get_digest() != hash_bv) {
      cout << "Hash does not match expected value." << endl;
      throw -1;
    }

    //generate proof using imported keypair
    const r1cs_ppzksnark_proof<ppT> proof = r1cs_ppzksnark_prover<ppT>(keypair.pk, pb.primary_input(), pb.auxiliary_input());
    _print_proof_to_file(proof, output_path+"/"+id+"_proof");

    return proof; 
}

int prover_from_files(string& pk_path, string& vk_path, string& left, string& right, string& hash, string& id, string& output_path)
{ 
  try
  {
    default_r1cs_ppzksnark_pp::init_public_params();
    r1cs_ppzksnark_verification_key<default_r1cs_ppzksnark_pp> vk = _retrieve_vk_from_file(vk_path.c_str());
    r1cs_ppzksnark_proving_key<default_r1cs_ppzksnark_pp> pk = _retrieve_pk_from_file(pk_path.c_str());
    r1cs_ppzksnark_keypair<default_r1cs_ppzksnark_pp> keypair = r1cs_ppzksnark_keypair<default_r1cs_ppzksnark_pp>(std::move(pk), std::move(vk));
    r1cs_ppzksnark_proof<default_r1cs_ppzksnark_pp> proof = prover(keypair, left, right, hash, id, output_path);
    return 1;
  }
  catch(int n)
  {
    return -1;
  }
  catch (const std::exception &exc)
  { 
    std::cerr << exc.what(); 
    return -1;
  }
  catch(...)
  {
    return -1;
  }
}


r1cs_ppzksnark_keypair<default_r1cs_ppzksnark_pp> generator(string& left, string& right, string& hash, string& id, string& output_path){
    const libff::bit_vector left_bv = string_to_bits(left);
    const libff::bit_vector right_bv = string_to_bits(right);
    const libff::bit_vector hash_bv = string_to_bits(hash);

    default_r1cs_ppzksnark_pp::init_public_params();
    typedef libsnark::default_r1cs_ppzksnark_pp ppT;
    typedef libff::Fr<ppT> FieldT;

    //setup protoboard
    protoboard<FieldT> pb;
    digest_variable<FieldT> hash_bits(pb, SHA256_digest_size, "hash_bits");
    pb.set_input_sizes(256); //first 256 bits are the public ones
    digest_variable<FieldT> left_bits(pb, SHA256_digest_size, "left_bits");
    digest_variable<FieldT> right_bits(pb, SHA256_digest_size, "right_bits");

    sha256_two_to_one_hash_gadget<FieldT> hasher(pb, left_bits, right_bits, hash_bits, "hash_gadget");
    hasher.generate_r1cs_constraints();

    //preapre witnesses
    left_bits.generate_r1cs_witness(left_bv);
    right_bits.generate_r1cs_witness(right_bv);

    hasher.generate_r1cs_witness();

    hash_bits.generate_r1cs_witness(hash_bv);
    assert(pb.is_satisfied());

    //data integrity check:
    if (hash_bits.get_digest() != hash_bv) {
        cout << "Hash does not match expected value." << endl;
        throw -1;
    }

    //generate the keys:
    const r1cs_constraint_system<FieldT> constraint_system = pb.get_constraint_system();
    const r1cs_ppzksnark_keypair<ppT> keypair = r1cs_ppzksnark_generator<ppT>(constraint_system);
    const r1cs_ppzksnark_proof<ppT> proof = r1cs_ppzksnark_prover<ppT>(keypair.pk, pb.primary_input(), pb.auxiliary_input());
    bool verified = r1cs_ppzksnark_verifier_strong_IC<ppT>(keypair.vk, pb.primary_input(), proof);
    const r1cs_ppzksnark_verification_key<ppT> vk = keypair.vk;

    const r1cs_ppzksnark_processed_verification_key<ppT> pvk = r1cs_ppzksnark_verifier_process_vk<ppT>(vk);
    _print_vk_to_file(vk, output_path+"/"+id+"_vk");
    _print_pvk_to_file(pvk, output_path+"/"+id+"_pvk");
    _print_pk_to_file(keypair.pk, output_path+"/"+id+"_pk");
    
    return keypair;
}



/* GMOLL example for writing vk to file:

template<typename ppT>
void _print_vk_to_file(r1cs_ppzksnark_verification_key<ppT> vk, string pathToFile)
{
  ofstream vk_data;
  vk_data.open(pathToFile);

  G2<ppT> A(vk.alphaA_g2);
  A.to_affine_coordinates();
  G1<ppT> B(vk.alphaB_g1);
  B.to_affine_coordinates();
  G2<ppT> C(vk.alphaC_g2);
  C.to_affine_coordinates();

  G2<ppT> gamma(vk.gamma_g2);
  gamma.to_affine_coordinates();
  G1<ppT> gamma_beta_1(vk.gamma_beta_g1);
  gamma_beta_1.to_affine_coordinates();
  G2<ppT> gamma_beta_2(vk.gamma_beta_g2);
  gamma_beta_2.to_affine_coordinates();

  G2<ppT> Z(vk.rC_Z_g2);
  Z.to_affine_coordinates();

  accumulation_vector<G1<ppT>> IC(vk.encoded_IC_query);
  G1<ppT> IC_0(IC.first);
  IC_0.to_affine_coordinates();

  vk_data << A.coord[0] << endl;
  vk_data << A.coord[1] << endl;

  vk_data << B.coord[0] << endl;
  vk_data << B.coord[1] << endl;

  vk_data << C.coord[0] << endl;
  vk_data << C.coord[1] << endl;

  vk_data << gamma.coord[0] << endl;
  vk_data << gamma.coord[1] << endl;

  vk_data << gamma_beta_1.coord[0] << endl;
  vk_data << gamma_beta_1.coord[1] << endl;

  vk_data << gamma_beta_2.coord[0] << endl;
  vk_data << gamma_beta_2.coord[1] << endl;

  vk_data << Z.coord[0] << endl;
  vk_data << Z.coord[1] << endl;

  vk_data << IC_0.coord[0] << endl;
  vk_data << IC_0.coord[1] << endl;

  for(size_t i=0; i<IC.size(); i++) {
    G1<ppT> IC_N(IC.rest[i]);
    IC_N.to_affine_coordinates();
    vk_data << IC_N.coord[0] << endl;
    vk_data << IC_N.coord[1] << endl;
  }

  vk_data.close();
}

*/
