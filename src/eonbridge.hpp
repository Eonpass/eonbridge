#include <string>
#include <libff/common/default_types/ec_pp.hpp>
#include <libsnark/common/default_types/r1cs_gg_ppzksnark_pp.hpp>
#include <libsnark/relations/constraint_satisfaction_problems/r1cs/examples/r1cs_examples.hpp>
#include <libsnark/zk_proof_systems/ppzksnark/r1cs_gg_ppzksnark/r1cs_gg_ppzksnark.hpp>

#include <libff/algebra/fields/field_utils.hpp>
#include <libff/common/default_types/ec_pp.hpp>
#include <libff/common/profiling.hpp>
#include <libff/common/utils.hpp>
#include <libsnark/common/default_types/r1cs_ppzksnark_pp.hpp>
#include <libsnark/gadgetlib1/gadgets/hashes/sha256/sha256_gadget.hpp>
#include <libsnark/gadgetlib1/pb_variable.hpp>
#include <libsnark/zk_proof_systems/ppzksnark/r1cs_ppzksnark/r1cs_ppzksnark.hpp>

#include "libff/algebra/curves/public_params.hpp"

//write and load keys and proofs:
libsnark::r1cs_ppzksnark_verification_key<libsnark::default_r1cs_ppzksnark_pp> _retrieve_vk_from_file(std::string pathToFile);
int _print_vk_to_file(libsnark::r1cs_ppzksnark_verification_key<libsnark::default_r1cs_ppzksnark_pp> vk, std::string pathToFile);

libsnark::r1cs_ppzksnark_processed_verification_key<libsnark::default_r1cs_ppzksnark_pp> _retrieve_pvk_from_file(std::string pathToFile);
int _print_pvk_to_file(libsnark::r1cs_ppzksnark_processed_verification_key<libsnark::default_r1cs_ppzksnark_pp> vk, std::string pathToFile);

libsnark::r1cs_ppzksnark_proving_key<libsnark::default_r1cs_ppzksnark_pp> _retrieve_pk_from_file(std::string pathToFile);
int _print_pk_to_file(libsnark::r1cs_ppzksnark_proving_key<libsnark::default_r1cs_ppzksnark_pp> proof, std::string pathToFile);

libsnark::r1cs_ppzksnark_proof<libsnark::default_r1cs_ppzksnark_pp> _retrieve_proof_from_file(std::string pathToFile);
int _print_proof_to_file(libsnark::r1cs_ppzksnark_proof<libsnark::default_r1cs_ppzksnark_pp> proof, std::string pathToFile);

libsnark::r1cs_primary_input<libff::Fr<libsnark::default_r1cs_ppzksnark_pp>> _retrieve_primary_input_from_file(std::string pathToFile);
int _print_primaryinput_to_file(libsnark::r1cs_primary_input<libff::Fr<libsnark::default_r1cs_ppzksnark_pp>> primary_input, std::string pathToFile);

//main components
libsnark::r1cs_ppzksnark_keypair<libsnark::default_r1cs_ppzksnark_pp> generator(std::string& left, std::string& right, std::string& hash, std::string& doc_id, std::string& output_path);

libsnark::r1cs_ppzksnark_proof<libsnark::default_r1cs_ppzksnark_pp> prover(libsnark::r1cs_ppzksnark_keypair<libsnark::default_r1cs_ppzksnark_pp> keypair, std::string& left, std::string& right, std::string& hash, std::string& id, std::string& output_path);
int prover_from_files(std::string& pk_path, std::string& vk_path, std::string& left, std::string& right, std::string& hash, std::string& id, std::string& output_path);

int verifier(libsnark::r1cs_ppzksnark_processed_verification_key<libsnark::default_r1cs_ppzksnark_pp> pvk, libsnark::r1cs_ppzksnark_proof<libsnark::default_r1cs_ppzksnark_pp> proof, std::string& hash, std::string& id);
int verifier_from_files(std::string& pvk_path, std::string& proof_path, std::string& hash, std::string& id);