#include <gtest/gtest.h>
#include <eonbridge.hpp> 


#include <libff/common/default_types/ec_pp.hpp>
#include <libsnark/common/default_types/r1cs_gg_ppzksnark_pp.hpp>
#include <libsnark/relations/constraint_satisfaction_problems/r1cs/examples/r1cs_examples.hpp>
#include <libsnark/zk_proof_systems/ppzksnark/r1cs_gg_ppzksnark/r1cs_gg_ppzksnark.hpp>

#include <libff/algebra/fields/field_utils.hpp>
#include <libff/common/default_types/ec_pp.hpp>
#include <libff/common/profiling.hpp>
#include <libff/common/utils.hpp>
#include <libsnark/common/default_types/r1cs_ppzksnark_pp.hpp>
#include <libsnark/gadgetlib1/gadgets/hashes/sha256/sha256_gadget.hpp>
#include <libsnark/gadgetlib1/pb_variable.hpp>
#include <libsnark/zk_proof_systems/ppzksnark/r1cs_ppzksnark/r1cs_ppzksnark.hpp>

#include "libff/algebra/curves/public_params.hpp"

using namespace std;
using namespace libsnark;

TEST(EonbridgeTest, TestGeneratorProverVerifier) {
  
    string left = std::string("0x426bc2d8,0x4dc86782,0x81e8957a,0x409ec148,0xe6cffbe8,0xafe6ba4f,0x9c6f1978,0xdd7af7e9");
    string right = std::string("0x038cce42,0xabd366b8,0x3ede7e00,0x9130de53,0x72cdf73d,0xee825114,0x8cb48d1b,0x9af68ad0");
    string hash  = std::string("0xeffd0b7f,0x1ccba116,0x2ee816f7,0x31c62b48,0x59305141,0x990e5c0a,0xce40d33d,0x0b1167d1");
    string test_id  = std::string("mytestid");
    string output_path  = std::string("../output");
    
    try
    {
        //generate keypairs:
        r1cs_ppzksnark_keypair<default_r1cs_ppzksnark_pp> keypair = generator(left,right,hash,test_id,output_path);
        r1cs_ppzksnark_verification_key<default_r1cs_ppzksnark_pp> vk = keypair.vk;
        r1cs_ppzksnark_proving_key<default_r1cs_ppzksnark_pp> pk = keypair.pk;
        
        //write keys to file to simulate passing the information between different entities
        _print_vk_to_file(vk, "../output/test/test_vk");
        r1cs_ppzksnark_verification_key<default_r1cs_ppzksnark_pp> vk2 = _retrieve_vk_from_file("../output/test/test_vk");
        EXPECT_TRUE(vk==vk2);

        r1cs_ppzksnark_processed_verification_key<default_r1cs_ppzksnark_pp> pvk = r1cs_ppzksnark_verifier_process_vk<default_r1cs_ppzksnark_pp>(vk);
        _print_pvk_to_file(pvk, "../output/test/test_pvk");
        r1cs_ppzksnark_processed_verification_key<default_r1cs_ppzksnark_pp> pvk2 = _retrieve_pvk_from_file("../output/test/test_pvk");
        EXPECT_TRUE(pvk==pvk2);
        
        _print_pk_to_file(pk, "../output/test/test_pk");
        r1cs_ppzksnark_proving_key<default_r1cs_ppzksnark_pp> pk2 = _retrieve_pk_from_file("../output/test/test_pk");
        EXPECT_TRUE(pk==pk2);

        r1cs_ppzksnark_keypair<default_r1cs_ppzksnark_pp> keypair2 = r1cs_ppzksnark_keypair<default_r1cs_ppzksnark_pp>(std::move(pk2), std::move(vk2));

        //pvk needs a special comparison since vk>>file doesn't port the IC query properly
        //i.e. pvk created by a vk reconstructed via file DOES NOT equals pvk created with the original vk
        //see my issue on github: https://github.com/scipr-lab/libsnark/issues/181
        r1cs_ppzksnark_processed_verification_key<default_r1cs_ppzksnark_pp> pvk_star = r1cs_ppzksnark_verifier_process_vk<default_r1cs_ppzksnark_pp>(vk2);
        EXPECT_FALSE(pvk_star==pvk);
        EXPECT_TRUE(pvk_star.vk_alphaA_g2_precomp == pvk.vk_alphaA_g2_precomp);
        EXPECT_TRUE(pvk_star.vk_alphaB_g1_precomp == pvk.vk_alphaB_g1_precomp);
        EXPECT_TRUE(pvk_star.vk_alphaC_g2_precomp == pvk.vk_alphaC_g2_precomp);
        EXPECT_TRUE(pvk_star.vk_gamma_g2_precomp == pvk.vk_gamma_g2_precomp);
        EXPECT_TRUE(pvk_star.vk_gamma_beta_g1_precomp == pvk.vk_gamma_beta_g1_precomp);
        EXPECT_TRUE(pvk_star.vk_gamma_beta_g2_precomp == pvk.vk_gamma_beta_g2_precomp);
        EXPECT_TRUE(pvk_star.vk_rC_Z_g2_precomp == pvk.vk_rC_Z_g2_precomp);
        EXPECT_FALSE(pvk_star.encoded_IC_query == pvk.encoded_IC_query);//!!!!!!

        //generate proof:
        // for whatever reason, keypair.pk has more information than pk only, maybe "r1cs_ppzksnark_prover" needs to access vk too, 
        // it does it via memory pointer, it's not clear from the documentation. Anyway, passing the proving key only results in segmentation fault
        r1cs_ppzksnark_proof<default_r1cs_ppzksnark_pp> proof = prover(keypair2,left,right,hash,test_id,output_path);

        _print_proof_to_file(proof, "../output/test/test_proof");
        r1cs_ppzksnark_proof<default_r1cs_ppzksnark_pp> proof2 = _retrieve_proof_from_file("../output/test/test_proof");
        EXPECT_TRUE(proof==proof2);

        //verify proof:
        //verify with pvk and proof as returned by functions
        bool verified = verifier(pvk, proof, hash, test_id);
        EXPECT_TRUE(verified);

        //verify with pvk and proof retrieved from files
        bool verified2 = verifier(pvk2, proof2, hash, test_id);
        EXPECT_TRUE(verified2);

        //remove the test files created by generator,prover and verifier from output folder:
        string file_types[4] = { "_pk", "_vk", "_pvk", "_proof" }; 
        for (int i = 0; i < 4; i++) 
        {
            string temp_file_name = "../output/"+test_id+file_types[i];
            if(remove(temp_file_name.c_str())!=0){
                cout << "Error removing temporary file" << endl;
            }
            temp_file_name = "../output/test/test"+file_types[i];
            if(remove(temp_file_name.c_str())!=0){
                cout << "Error removing temporary file" << endl;
            }
        } 
        

    }
    catch(int n)
    {
        FAIL();
    }
    catch(...)
    {
        FAIL();
    }

}

TEST(EonbridgeTest, TestGeneratorProverVerifierCommandLineMethods) {
  
    string left = std::string("0x426bc2d8,0x4dc86782,0x81e8957a,0x409ec148,0xe6cffbe8,0xafe6ba4f,0x9c6f1978,0xdd7af7e9");
    string right = std::string("0x038cce42,0xabd366b8,0x3ede7e00,0x9130de53,0x72cdf73d,0xee825114,0x8cb48d1b,0x9af68ad0");
    string hash  = std::string("0xeffd0b7f,0x1ccba116,0x2ee816f7,0x31c62b48,0x59305141,0x990e5c0a,0xce40d33d,0x0b1167d1");
    string test_id  = std::string("mytestid2");
    string output_path  = std::string("../output");
    string pk_path = "../output/"+test_id+"_pk";
    string vk_path = "../output/"+test_id+"_vk";
    string pvk_path = "../output/"+test_id+"_pvk";
    string proof_path = "../output/"+test_id+"_proof";

    
    try
    {
        //generate keypairs:
        r1cs_ppzksnark_keypair<default_r1cs_ppzksnark_pp> keypair = generator(left,right,hash,test_id,output_path);
        //check generator created the files:
        if (FILE *file = fopen(pk_path.c_str(), "r")) {
            fclose(file);
            EXPECT_TRUE(true);
        } else {
            EXPECT_TRUE(false);
        }  
        //generate proof: 
        int prover_outcome = prover_from_files(pk_path ,vk_path, left, right, hash, test_id, output_path);
        EXPECT_TRUE(prover_outcome == 1);
        //check prover created the files:
        if (FILE *file = fopen(proof_path.c_str(), "r")) {
            fclose(file);
            EXPECT_TRUE(true);
        } else {
            EXPECT_TRUE(false);
        }   
        //verify proof:
        int verifier_outcome = verifier_from_files(pvk_path ,proof_path, hash, test_id);
        EXPECT_TRUE(verifier_outcome == 1);

        //remove the test files created by generator,prover and verifier from output folder:
        string file_types[4] = { "_pk", "_vk", "_pvk", "_proof" }; 
        for (int i = 0; i < 4; i++) 
        {
            string temp_file_name = "../output/"+test_id+file_types[i];
            if(remove(temp_file_name.c_str())!=0){
                cout << "Error removing temporary file" << endl;
            }
        }  

    }
    catch(int n)
    {
        FAIL();
    }
    catch(...)
    {
        FAIL();
    }

}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}